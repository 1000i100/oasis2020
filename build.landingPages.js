const fs = require("fs");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const jimp = require('jimp');
const jimpConfigure = require('@jimp/custom');
const jimpPlugins = require('@jimp/plugins');
const jimpCircle = require('@jimp/plugin-circle');
jimpConfigure({
	plugins: [jimpPlugins,jimpCircle]
});
const conf = require('js-yaml').safeLoad(fs.readFileSync('event.config.yml', 'utf8'));

const bandeauCache = {};
const composeFunc = {
	1:async (base,imgSpeakers)=>await composeRightTop(base,imgSpeakers[0],400,150,20),
	2:async (base,imgSpeakers)=>{
		await composeRightTop(base,imgSpeakers[1],350,20,40);
		await composeRightTop(base,imgSpeakers[0],350,350+(2*20),40);
	},
	3:async (base,imgSpeakers)=>{
		await composeRightTop(base,imgSpeakers[2],250,20,140);
		await composeRightTop(base,imgSpeakers[1],250,260,20);
		await composeRightTop(base,imgSpeakers[0],250,500,140);
		// await composeRightTop(base,imgSpeakers[2],250,20,20);
		// await composeRightTop(base,imgSpeakers[1],250,530/2+20,20+55);
		// await composeRightTop(base,imgSpeakers[0],250,550,130);
	},
	4:async (base,imgSpeakers)=>{
		await composeRightTop(base,imgSpeakers[3],250,5,5);
		await composeRightTop(base,imgSpeakers[2],250,374/2+5,180);
		await composeRightTop(base,imgSpeakers[1],250,379,5);
		await composeRightTop(base,imgSpeakers[0],250,603,133);
	},
	5:async (base,imgSpeakers)=>{
		await composeRightTop(base,imgSpeakers[3],200,(620-20)/3-50,50);
		await composeRightTop(base,imgSpeakers[1],200,2*(620-20)/3+50,20);
		await composeRightTop(base,imgSpeakers[4],200,20,230);
		await composeRightTop(base,imgSpeakers[2],200,(620-20)/2+20,200);
		await composeRightTop(base,imgSpeakers[0],200,620,170);
	},
	6:async (base,imgSpeakers)=>{
		const max = 1090;
		const min = 440;
		const step = (s)=>s*(max-min)/5+min;
		await compose(base,imgSpeakers[0],200,step(0),270);
		await compose(base,imgSpeakers[1],200,step(1)+20,120);
		await compose(base,imgSpeakers[2],200,step(2)-10,300);
		await compose(base,imgSpeakers[3],200,step(3)+10,150);
		await compose(base,imgSpeakers[4],200,step(4)-20,330);
		await compose(base,imgSpeakers[5],200,step(5),180);
	},
	7:async (base,imgSpeakers)=> {
		const size = 175;
		const Xmarge = 10;
		const Ymarge = 10;
		const Xmin = 350 + size / 2 + Xmarge;
		const Xmax = 1200 - size / 2 - Xmarge;
		const Xstep = (s) => s * (Xmax - Xmin) / 4.1 + Xmin;
		const Ymin = 20+size / 2 + Ymarge;
		const Ymax = 400 - size / 2 - Xmarge;
		const Ystep = (s) => s * (Ymax - Ymin) / 2 + Ymin;
		await compose(base, imgSpeakers[0], size, Xstep(0), Ystep(1));
		await compose(base, imgSpeakers[1], size, Xstep(1.05), Ystep(0));
		await compose(base, imgSpeakers[2], size, Xstep(1), Ystep(2.2));
		await compose(base, imgSpeakers[3], size, Xstep(2.05), Ystep(1.2));
		await compose(base, imgSpeakers[4], size, Xstep(3.1), Ystep(0.2));
		await compose(base, imgSpeakers[5], size, Xstep(3.05), Ystep(2.4));
		await compose(base, imgSpeakers[6], size, Xstep(4.1), Ystep(1.4));
	},
	8:async (base,imgSpeakers)=>{
		const size = 160;
		const Xmarge = 15;
		const Ymarge = 10;
		const Xmin = 480 + size/2 + Xmarge;
		const Xmax = 1200 - size/2 - Xmarge;
		const Xstep = (s)=>s*(Xmax-Xmin)/3+Xmin;
		const Ymin = 30+size/2 + Ymarge;
		const Ymax = 400 - size/2 - Xmarge;
		const Ystep = (s)=>s*(Ymax-Ymin)/1.1+Ymin;
		await compose(base,imgSpeakers[0],size,Xstep(0),Ystep(0));
		await compose(base,imgSpeakers[1],size,Xstep(-0.5),Ystep(1));
		await compose(base,imgSpeakers[2],size,Xstep(1),Ystep(0.1));
		await compose(base,imgSpeakers[3],size,Xstep(0.5),Ystep(1.1));
		await compose(base,imgSpeakers[4],size,Xstep(2),Ystep(0.2));
		await compose(base,imgSpeakers[5],size,Xstep(1.5),Ystep(1.2));
		await compose(base,imgSpeakers[6],size,Xstep(3),Ystep(0.3));
		await compose(base,imgSpeakers[7],size,Xstep(2.5),Ystep(1.3));
	},
	9:async (base,imgSpeakers)=>{
		const size = 150;
		const Xmarge = 10;
		const Ymarge = 10;
		const Xmin = 330 + size/2 + Xmarge;
		const Xmax = 1200 - size/2 - Xmarge;
		const Xstep = (s)=>s*(Xmax-Xmin)/7+Xmin;
		const Ymin = size/2 + Ymarge;
		const Ymax = 380 - size/2 - Xmarge;
		const Ystep = (s)=>s*(Ymax-Ymin)/2+Ymin;
		await compose(base,imgSpeakers[0],size,Xstep(0),Ystep(2));
		await compose(base,imgSpeakers[1],size,Xstep(1.25),Ystep(1));
		await compose(base,imgSpeakers[2],size,Xstep(2.5),Ystep(0));
		await compose(base,imgSpeakers[3],size,Xstep(2.25),Ystep(2.2));
		await compose(base,imgSpeakers[4],size,Xstep(3.5),Ystep(1.2));
		await compose(base,imgSpeakers[5],size,Xstep(4.75),Ystep(0.2));
		await compose(base,imgSpeakers[6],size,Xstep(4.5),Ystep(2.4));
		await compose(base,imgSpeakers[7],size,Xstep(5.75),Ystep(1.4));
		await compose(base,imgSpeakers[8],size,Xstep(7),Ystep(0.4));
	},
	10:async (base,imgSpeakers)=>{
		const size = 150;
		const Xmarge = 10;
		const Ymarge = 10;
		const Xmin = 330 + size/2 + Xmarge;
		const Xmax = 1200 - size/2 - Xmarge;
		const Xstep = (s)=>s*(Xmax-Xmin)/7+Xmin;
		const Ymin = size/2 + Ymarge;
		const Ymax = 380 - size/2 - Xmarge;
		const Ystep = (s)=>s*(Ymax-Ymin)/2+Ymin;
		await compose(base,imgSpeakers[0],size,Xstep(0),Ystep(2));
		await compose(base,imgSpeakers[1],size,Xstep(1.25),Ystep(1));
		await compose(base,imgSpeakers[2],size,Xstep(2.5),Ystep(0));
		await compose(base,imgSpeakers[3],size,Xstep(2.25),Ystep(2.2));
		await compose(base,imgSpeakers[4],size,Xstep(3.5),Ystep(1.2));
		await compose(base,imgSpeakers[5],size,Xstep(4.75),Ystep(0.2));
		await compose(base,imgSpeakers[6],size,Xstep(4.5),Ystep(2.4));
		await compose(base,imgSpeakers[7],size,Xstep(5.75),Ystep(1.4));
		await compose(base,imgSpeakers[8],size,Xstep(7),Ystep(0.4));
		await compose(base,imgSpeakers[9],size,Xstep(6.75),Ystep(2.6));
	}
	//3:async (base,imgSpeakers)=>{
	//},

};
let mainBase;
async function buildBandeau(name, imgSpeakers) {
	if(bandeauCache[name]) return;
	bandeauCache[name] = true;
	if(!imgSpeakers.length) return;
	if(!mainBase) mainBase = await (await jimp.read('template/bandeau_speaker.png')).resize(1200,jimp.AUTO);
	const base = await mainBase.clone();
	if(composeFunc[imgSpeakers.length]) await composeFunc[imgSpeakers.length](base,imgSpeakers);
	else console.log(imgSpeakers.length, imgSpeakers);
	await base.write(`generated.public/img/social_media/${name}`);
}
async function composeRightTop(base,imgFile,size,right,top=20){
	const speaker = await jimp.read(`static/${imgFile}`);
	await speaker.resize(size,size).circle();
	await base.blit(speaker, 1200-right-size, top);
}
async function compose(base,imgFile,size,x,y){
	const speaker = await jimp.read(`static/${imgFile}`);
	await speaker.resize(size,size).circle();
	await base.blit(speaker, x-size/2, y-size/2);
}

const dom = file2dom('generated.public/full_site.html');
const navArea = dom.window.document.querySelector("header").innerHTML; //#mainMenu

const subPages = subPageExtractor(dom.window.document);
updateSiteMap(subPages.map(subPage=>dom2siteMapLine(subPage)));
subPages.forEach(dom2landingPage);

function updateSiteMap(tab){
	let sitemapContent = fs.readFileSync('generated.public/sitemap.xml', 'utf8');
	sitemapContent = `${sitemapContent.split("</urlset>")[0]}${tab.join("\n")}\n</urlset>`;
	fs.writeFileSync(`generated.public/sitemap.xml`,sitemapContent,"utf8");
}
function file2dom(fileName) {
	return new JSDOM(fs.readFileSync(`${fileName}`,"utf8"));
}
function subPageExtractor(document) {
	const subPages = document.querySelectorAll("[id]");
	const pagesArray = Object.keys(subPages).map((k)=>subPages[k]);
    return pagesArray.filter(n=>n.id.indexOf('-') === -1);
}
function getTitle(domPiece){
	const title = domPiece.querySelector('.ogTitle');
	if(title && title.value) return title.value.trim();
	for(let i=1;i<=6;i++){
		let title = domPiece.querySelector(`h${i}`);
		if(title !== null) {
			return title.textContent;
		}
	}
	return domPiece.getAttribute('id');
}
function getDescription(domPiece){
	const ogDesc = domPiece.querySelector('.ogDesc');
	if(ogDesc && ogDesc.value) return ogDesc.value.trim();
	let descDoms = domPiece.querySelectorAll('.desc');
	if(!descDoms.length) descDoms = [domPiece];
	let desc = '';
	for(let i in descDoms){
		desc += descDoms[i].textContent?descDoms[i].textContent+' ':'';
	}
  if(domPiece.querySelectorAll('.video').length === 1) desc = `Vidéo maintenant disponible ! ${desc}`;
	return desc.replace(/\s+/g," ").trim().substring(0,320);
}
function dedup(array) {
	const res = {};
	array.forEach(v=>res[v]=v);
	return Object.keys(res);
}
function getAvatars(domPiece){
	const ogAvatars = domPiece.querySelector('.ogAvatars');
	if(ogAvatars && ogAvatars.value) return ogAvatars.value.split(" ");
	let avatarDoms = domPiece.querySelectorAll('.avatar');
	return imgNodes2array(avatarDoms);
}
function imgNodes2array(imgNodes){
	return dedup(Object.values(imgNodes).map(n=>`${n.src}§${n.title||n.alt}`)).map(str=>{
		const tab = str.split('§');
		return {src:tab[0],alt:tab[1]}
	});
}
function dom2siteMapLine(domPiece){
	const subPageId = domPiece.getAttribute('id');
	const imgNodes = domPiece.querySelectorAll('img');
	return `    <url>
        <loc>${conf.canonical}${subPageId}.html</loc>${imgNodes2array(imgNodes).map(n=>`
        <image:image>
            <image:loc>${conf.canonical}${n.src}</image:loc>
            <image:caption>${escapeHtml(n.alt)}</image:caption>
        </image:image>`).join('')}
    </url>`;
}
function escapeHtml(unsafe) {
	return unsafe
		.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;")
		.replace(/"/g, "&quot;")
		.replace(/'/g, "&#039;");
}
function computeBannerFileName(avatars) {
	if(!avatars.length) return 'RML_all_in_one.jpg';
	let name = `session__${avatars.map(a=>a.src.substring(a.src.lastIndexOf('/')+1,a.src.lastIndexOf('.'))).join('__')}.jpg`;
	if(name.length>250) name = `${avatars.map(a=>a.src.substring(a.src.lastIndexOf('/')+1,a.src.lastIndexOf('.')).split('_')[0]).join('_')}.jpg`;
	if(avatars.length>10) return 'RML_all_in_one.jpg';//TODO: à corriger en même temps que le générateur de bandeau
	return name;
}
function dom2landingPage(domPiece) {
	const subPageId = domPiece.getAttribute('id');
	const title = getTitle(domPiece);
	const description = getDescription(domPiece);
	const avatars = getAvatars(domPiece);
	const bandeauName = computeBannerFileName(avatars);
	buildBandeau(bandeauName, avatars.map(a=>a.src));
	domPiece.classList.add('active');

	const landingPageDom = new JSDOM(`
<!DOCTYPE html>
<html lang="fr" xmlns:og="http://ogp.me/ns#" vocab="http://schema.org/">
<head prefix="og: http://ogp.me/ns# website: http://ogp.me/ns/website#">
    <meta charset="utf-8"/>
    <title>${title} ~ ${conf.titre.tiny}</title>
    <link rel="stylesheet" href="style.css"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>

    <link rel="icon" href="img/logo/g16.png" type="image/png"/>
    <link rel="icon" sizes="32x32" href="img/logo/g32.png" type="image/png">
    <link rel="icon" sizes="64x64" href="img/logo/g64.png" type="image/png">
    <link rel="icon" sizes="96x96" href="img/logo/g96.png" type="image/png">
    <link rel="icon" sizes="196x196" href="img/logo/g196.png" type="image/png">
    <link rel="icon" sizes="1000x1000" href="img/logo/g1000.png" type="image/png"/>
    <link rel="icon" sizes="any" href="img/logo/g.svg" type="image/svg+xml"/>

    <link rel="apple-touch-icon" sizes="152x152" href="img/logo/g152.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/logo/g60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/logo/g76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/logo/g114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/logo/g120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/logo/g144.png">

    <meta name="msapplication-TileImage" content="img/logo/g144.png">
    <meta name="msapplication-TileColor" content="#FFFFFF">

    <link rel="canonical" href="${conf.canonical}${subPageId}.html"/>
    <link type="text/plain" rel="author" href="${conf.canonical}humans.txt" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="manifest.json"/>
    <meta name="theme-color" content="rgb(255,198,84)"/>

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="fr_FR" />
    <meta property="og:title" content="${title}" />
    <meta property="og:site_name" content="{conf:og.site_name}" />
    <meta property="og:url" content="${conf.canonical}${subPageId}.html" />

    <meta property="og:image" content="${conf.canonical}${conf.og.images_path}${bandeauName}" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="628" />
    ${avatars.map(a=>`
    <meta property="og:image" content="${conf.canonical}${a.src}" />
    <meta property="og:image:alt" content="${a.alt}" />
    <meta property="og:image:width" content="250" />
    <meta property="og:image:height" content="250" />
    `).join('')}
    <meta property="og:description" content="${description}" />
    <meta name="description" content="${description}" />
	<script src="main.js" defer></script>
</head>
<body>
<header>
${navArea}
</header>
<noscript>
	Sans JavaScript ? &gt;&gt; <a href="./full_site.html#${subPageId}">${title}</a>
</noscript>
</body>
</html>
	`);
	domPiece.classList.add('active');
	landingPageDom.window.document.body.appendChild(domPiece);
	landingPageDom.window.document.body.appendChild(
(new JSDOM(`<div>
<script>
async function loadFullPage(){
	const page = await(await fetch('./full_site.html')).text();
	page.replace(new RegExp("<body>([^]+)</body>"),(fullBody,innerBody)=>document.body.innerHTML=innerBody);
	window.runJS();
}
/*
async function swInit(){
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('service_worker.js').then(
      (reg)=>{
       console.log('Service worker accepté', reg);
       navigator.serviceWorker.ready.then(loadFullPage);
      }, (err)=>{
        console.log('Service worker refusé', err);
        loadFullPage()
      });
  } else {
    console.log('Service workers are not supported.');
    loadFullPage();
  }
}
swInit();
*/ loadFullPage();

function bgCloseBuilder(side){
	const bgClose = document.createElement("a");
	bgClose.classList.add('bg'+side);
	bgClose.classList.add("bgPopupClose");
	bgClose.href = "./";
	bgClose.title = "Fermer";
	return bgClose;
}
document.querySelectorAll('.subPage').forEach( popup => {
	const fermer = document.createElement("a");
	fermer.classList.add("close");
	fermer.href = "./";
	fermer.title = "Fermer";
	fermer.innerHTML = "X";
	popup.appendChild(fermer);
	popup.appendChild(bgCloseBuilder('Top'));
	popup.appendChild(bgCloseBuilder('Left'));
	popup.appendChild(bgCloseBuilder('Right'));
	popup.appendChild(bgCloseBuilder('Bottom'));
} );
</script></div>
`)).window.document.body.querySelector('div'));
	let pageStr = landingPageDom.serialize();
	const pageParts = pageStr.split(' id="');
	const part0 = pageParts.shift();
	const part1 = pageParts.shift();
	let otherParts = pageParts.join(' id="');
	otherParts = otherParts.replace(/<script type="application\/ld\+json">((?!<\/script>)[^])*<\/script>/g,'');
	if(otherParts) pageStr=`${part0} id="${part1} id="${otherParts}`;
	else pageStr=`${part0} id="${part1}`;
    fs.writeFileSync(`generated.public/${subPageId}.html`,pageStr,"utf8");
}
