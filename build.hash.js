const fs = require("fs");
const md5 = require("md5");
const yaml = require('js-yaml');

const conf = yaml.safeLoad(fs.readFileSync('event.config.yml', 'utf8'));

const cache={};

parseFile('style.css');
parseFile('manifest.json');
fs.readdirSync("generated.public/").filter(f=>f.indexOf('.html')!== -1).forEach(parseFile);
//fs.readdirSync("generated.public/").filter(f=>f.indexOf('.html')!== -1).forEach((filePath)=>cache[filePath]=filePath);
const fileToCache = Object.keys(cache).map(k=>cache[k].indexOf('/')===0?cache[k].slice(1):cache[k])
  .filter(str=>str.match(/(g[0-9]{3,4}\.png|press_HD|social_media|words_cloud|rml_splash|sessions_assets)/));
fileToCache.shift('full_site.html');

function parseFile(filePath){
	const fileContent = fs.readFileSync(`generated.public/${filePath}`,"utf8");
	fs.writeFileSync(`generated.public/${filePath}`,fileContent.replace(/"[^"]+\.[a-z]{2,4}"/g,str=>{
		if(str.indexOf('#')!== -1) return str;
		if(str.indexOf('@')!== -1 && str.indexOf('.jpg')=== -1 && str.indexOf('.png')=== -1) return str;
		if(str.indexOf('.html')!== -1) return str;
		if(str.indexOf('.ics')!== -1) return str;
		if(str.indexOf('/favicon.ico')!== -1) return str;
		if(str.indexOf('humans.txt')!== -1) return str;
		if(str.indexOf(`"${conf.canonical}`)!== -1) return `"${conf.canonical}${transformFile(str.slice(26,-1))}"`;
		if(str.indexOf(':')!== -1) return str;
		if(str.indexOf('//')!== -1) return str;
		return `"${transformFile(str.slice(1,-1))}"`;
	}),"utf8");

}
function transformFile(path){
	const oldPath = path;
	if(cache[oldPath]) return cache[oldPath];

	const hash = file2hash(`generated.public/${oldPath}`);
	const newPath = insertHashInFileName(hash,oldPath);
	fs.renameSync(`generated.public/${oldPath}`, `generated.public/${newPath}`);
	cache[oldPath] = newPath;
	return newPath;

}
function file2hash(fileName) {
	return str2hash(fs.readFileSync(`${fileName}`));
}
function str2hash(str) {
  return md5(str).slice(0,8);
}
function insertHashInFileName(hash,fileName){
  const beforeExt = fileName.slice(0,fileName.lastIndexOf('.'));
  const ext = fileName.slice(fileName.lastIndexOf('.')+1);
  return `${beforeExt}.${hash}.${ext}`;
}

/*
function buildSW(swFileName,replacement) {
  let fileContent = fs.readFileSync(`template/${swFileName}`,"utf8");
  fileContent = fileContent.replace(new RegExp('{build:fileToCache}','g'),()=>replacement);
  const hash = str2hash(fileContent);
  const swFileNameWithHash = insertHashInFileName(hash,swFileName);
  fileContent = fileContent.replace(new RegExp('swHashVersion','g'),()=>hash);
  fs.writeFileSync(`generated.public/${swFileNameWithHash}`,fileContent,"utf8");

  fs.readdirSync("generated.public/").filter(f=>f.indexOf('.html')!== -1).forEach((fileName)=>{
    let fileContent = fs.readFileSync(`generated.public/${fileName}`,"utf8");
    fileContent = fileContent.replace(new RegExp(swFileName,'g'),()=>swFileNameWithHash);
    fs.writeFileSync(`generated.public/${fileName}`,fileContent,"utf8");
  });
}

buildSW('service_worker.js',fileToCache.map(f=>`"${f}"`).join(','));
*/