
l'adaptation du contenu se limite à changer le contenu des dossiers et fichiers :
- `programme/`
- `template/pages/`
- `template/index.html`
- `event.config.yml`

Et potentiellement changer quelques contenu dans `static/img/`

Le reste devrait rouler avec :
- pour tester en local : après install de node.js et npm : `npm install` suivi de `npm run watch` dans le dossier du projet.
- pour mettre en prod : un push sur un dépot pour le quel j'ai configuré la bonne CI/CD

Pour remettre les vidéos en l'absence du programme:
soit éditer le fichier template/main.es6 (lignes 386-397)
soit ajouter en dur dans template/pages/splash.html

Pour éditer les styles:
fichier template/style.styl


# Cococo & Co
- Site accessible à l'adresse : https://cococo-et-co.com
- Urls de secours : https://www.cococo-et-co.com
- Urls de secours : https://1000i100.frama.io/oasis2020/
