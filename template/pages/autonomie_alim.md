# Autonomie alimentaire

Produire une nourriture sur un sol vivant, respectant l'équilibre spontané que nous offre la biodiversité. Utiliser des techniques comme l'agroécologie et la permaculture et s'appuyer sur le savoir-faire d'agriculteurs maraîchers.