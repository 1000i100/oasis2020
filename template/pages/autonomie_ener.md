# Autonomie énergétique

Viser le zéro-impact, c'est à dire repenser nos besoins en énergie et construire des habitats passifs et positifs, totalement intégrés dans l'environnement naturel. Créer un mix énergétique propre. 
Si nous devions quitter les lieux, nous aimerions laisser la nature qui nous a accueilli en meilleur état que nous l'avons trouvée en arrivant. 😉