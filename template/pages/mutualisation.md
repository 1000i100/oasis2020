# Mutualisation

Un modèle économique coopératif, la mutualisation des biens, des savoirs, de la joie et du faire-ensemble.