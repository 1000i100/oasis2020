# Gouvernance partagée en intelligence collective

Une gouvernance qui permet de développer notre co-responsabilité et de mettre le pouvoir de l'intelligence collective au service du succès de l'organisation. 
Nous sommes tous co-responsables des décisions à prendre, des décisions prises et de leur application, co-responsables du projet et du groupe, chacun s’attribue des rôles spécifiques dont il est le garant. L’organisation prend la forme de cercles interreliés qui permettent un fonctionnement souple et transparent.