# Espace presse & médias

## Communiqué de presse

<div class="page">

### 12ème Rencontres Monnaie Libre ( [#RML12](https://mastodon.social/tags/rml12) ) à Bordeaux du 17 au 25 novembre 2018

Le plus grand rassemblement de penseurs, économistes, informaticiens, utilisateurs et simples curieux de Monnaie Libre
aura lieu durant cette 12ème édition ( https://rml12.duniter.io/ ) qui se déroulera pour la première fois à **Bordeaux**
du **17** au **25 novembre** 2018. Entrée libre.

En 9 jours nous irons de la découverte ludique des alternatives monétaires aux conférences techniques
avec les développeurs experts. Parmi les participants interviendront Stéphane Laborde, Carole Lipsyc,
Jean-François Noubel, Etienne Chouard…

La Ğ1 (prononcez June), créée en mars 2017, est une monnaie __décentralisée__ dont les écritures sont enregistrées
dans une _blockchain__ peu énergivore et dont la __création est quotidiennement distribuée__ aux membres de la __Toile de
confiance__. Pour faire partie de cette Toile de confiance il vous suffit d’être parrainé par quelques membres.
Par leur parrainage (certification) ils attestent que vous êtes humain, unique et prêt à respecter le processus
de développement de cette toile et de la Ğ1. Une fois membre une part égale de création monétaire vous est attribuée
quotidiennement par le système : le Dividende Universel (DU).<br/>
Pour réguler cette monnaie au cours du temps et de sa croissance, le montant du DU augmente régulièrement et d’autres
paramètres sont appelés à évoluer en fonction du nombre de membres et de la masse monétaire
(cf [site 2](http://trm.creationmonetaire.info)).
La Ğ1 peut-être utilisée par quiconque (cf site [5](https://www.gchange.fr), [6](https://gannonce.duniter.org)),
membre ou non de la Toile de confiance, mais seuls les membres de celle-ci créent de la monnaie quotidiennement.

Ces 12ème Rencontres de la Monnaie Libre sont l’occasion pour __le public__ de découvrir cette monnaie décentralisée,
plus humaine, plus locale et qui rend à l’argent la fonction qu’il n’aurait jamais dû perdre, à savoir être un outil
permettant l’échange et le développement de tous sans exclusion.

C’est l’occasion pour les __créateurs__ de cette monnaie et les __informaticiens__ d’échanger et de préparer
les prochains développements qui soutiendront la croissance de la Ğ1 en toute sécurité
(cf [site 4](https://duniter.org/fr/). C’est également pour les __penseurs__, __philosophes__ et __économistes__
le moment de confronter leurs idées, analyses et prospectives
(cf site [2](http://trm.creationmonetaire.info),[3](http://www.fdlm.eu)).

Les rencontres se feront dans divers lieux de Bordeaux ;
bibliothèque Flora Tristan, Halle des Douves, l’Athénée municipal Joseph Wresinski…
(cf [site 1](https://rml12.duniter.io/))

#### Sites Références :

1. Le site des RML 12 : https://rml12.duniter.io/
2. Théorie Relative de la Monnaie : http://trm.creationmonetaire.info
3. Monnaie Libre Bordeaux : http://www.fdlm.eu
4. L’environnement de la Ğ1 : https://duniter.org/fr/
5. Plates-formes d’échanges : https://www.gchange.fr
6. Plates-formes d’échanges/financement : https://gannonce.duniter.org

#### Réseaux sociaux :

- Youtube (retransmission) : https://www.youtube.com/channel/UCTFiEd7f38oSl-Bo5HqJzyg
- Facebook : https://www.facebook.com/MonnaieLibreBordeaux/

#### Contact presse :

Pierre-Olivier : 06 09 33 13 61 / po@encrebleue.com

</div>

<!--[Dossier de presse](rencontres-monnaie-libre-bordeaux-rml12-dossier-presse.pdf)-->

## Visuels

### Logos
<div class="flexArea">
	<figure><img src="img/press/g.jpg" alt="Favicon site RML12">
		<figcaption>Favicon site RML12<br/>
			<a href="img/press_HD/g1000.png">HDpng</a>,<a href="img/press_HD/g.svg">svg</a>
		</figcaption>
	</figure>
	<figure><img src="img/press/logo-g1-2018-cc-00.jpg" alt="Ǧ1June 2018"/>
	    <figcaption>Ǧ1June 2018<br/>
	        <a href="img/press_HD/logo-g1-2018-cc-00.png">HDpng</a>,
	        <a href="img/press_HD/logo-g1-2018-cc-00.svg">svg</a>
	    </figcaption>
	</figure>
	<figure><img src="img/press/Logo-g1.flare.jpg" alt="Ǧ1 classique"/>
	    <figcaption>Ǧ1 classique<br/>
	        <a href="img/press_HD/Logo-g1.flare.png">HDpng</a>,
	        <a href="img/press_HD/Logo-g1.flare.svg">svg</a>
	    </figcaption>
	</figure>
</div>

### Stickers
<div class="flexArea">
	<figure><img src="img/press/Grml12.jpg" alt="Sticker Ǧ1 RML12"/>
	    <figcaption>
	        Sticker Ǧ1 RML12<br/>
	        <a href="img/press_HD/Grml12.png">HDpng</a>,
	        <a href="img/press_HD/Grml12.svg">svg</a>,
	        <a href="img/press_HD/flyer-Grml12-par14.pdf">pdf <sup>14</sup>/<sub>A4</sub></a>
	    </figcaption>
	</figure>
	<figure><img src="img/press/RML12-macaron.jpg" alt="Sticker tampon RML12"/>
	    <figcaption>Sticker tampon RML12<br/>
	        <a href="img/press_HD/RML12-macaron.png">HDpng</a>,
	        <a href="img/press_HD/RML12-macaron.svg">svg</a>,
	        <a href="img/press_HD/flyer-macaron-par11.pdf">pdf <sup>11</sup>/<sub>A4</sub></a>
	    </figcaption>
	</figure>
	<figure><img src="img/press/RML12-macaron-OpenDyslexic.jpg" alt="Sticker pour dyslexique"/>
	    <figcaption>Sticker pour dyslexique<br/>
	        <a href="img/press_HD/RML12-macaron-OpenDyslexic.png">HDpng</a>,
	        <a href="img/press_HD/RML12-macaron-OpenDyslexic.svg">svg</a>,
	        <a href="img/press_HD/flyer-macaron-par11.pdf">pdf <sup>11</sup>/<sub>A4</sub></a>
	    </figcaption>
	</figure>
</div>

### Bannière pour réseaux sociaux
<div class="flexArea">
	<figure><img src="img/press/RML12_social_compact_blanc.jpg" alt="Bandeau RML12 pour réseaux sociaux, base blanche"/>
	    <figcaption>Base blanche<br/>
	        <a href="img/press_HD/RML12_social_compact_blanc.jpg">HDjpg</a>,
	        <a href="img/press_HD/flyer-social-par10.pdf">pdf <sup>10</sup>/<sub>A4</sub></a>
	    </figcaption>
	</figure>
	<figure><img src="img/press/RML12_social_compact_noir.jpg" alt="Bandeau RML12 pour réseaux sociaux, base noir"/>
	    <figcaption>Base noire<br/>
	        <a href="img/press_HD/RML12_social_compact_noir.jpg">HDjpg</a>
	    </figcaption>
	</figure>
	<figure><img src="img/press/RML12_social_compact_transparent.jpg" alt="Bandeau RML12 pour réseaux sociaux, variante transparente"/>
	    <figcaption>Variante transparente<br/>
	        <a href="img/press_HD/RML12_social_compact_transparent.jpg">HDjpg</a>
	    </figcaption>
	</figure>
</div>

### Affiches / flyers
<div class="flexArea">
	<figure><img src="img/press/RML12-affiche-flyer-couleur.jpg" alt="Affiche ou flyer RML12 couleur"/>
	    <figcaption>Affiche ou flyer RML12 couleur<br/>
	        <a href="img/press_HD/RML12-affiche-flyer-couleur.jpg">HDjpg</a>,
	        <a href="img/press_HD/flyer-couleur-par4.pdf">pdf <sup>4</sup>/<sub>A4</sub></a>,
	        <a href="img/press_HD/flyer-couleur-par8.pdf">pdf <sup>8</sup>/<sub>A4</sub></a>
	    </figcaption>
	</figure>
	<figure><img src="img/press/RML12-affiche-flyer-NB.jpg" alt="Affiche ou flyer RML12 N&amp;B"/>
		<figcaption>Affiche ou flyer RML12 N&amp;B<br/>
			<a href="img/press_HD/RML12-affiche-flyer-NB.jpg">HDjpg</a>,
			<a href="img/press_HD/flyer-NB-par4.pdf">pdf <sup>4</sup>/<sub>A4</sub></a>,
			<a href="img/press_HD/flyer-NB-par8.pdf">pdf <sup>8</sup>/<sub>A4</sub></a>
		</figcaption>
	</figure>
	<figure><img src="img/press/visuel_tous_publics.jpg" alt="Visuel tous publics RML12"/>
		<figcaption>Visuel tous publics<br/>
			<a href="img/press_HD/visuel_tous_publics.png">HDpng</a>
		</figcaption>
	</figure>
	<figure><img src="img/press/affiche_tous_publics.jpg" alt="Affiche tous publics RML12"/>
		<figcaption>Affiche tous publics<br/>
			<a href="img/press_HD/affiche_tous_publics.png">HDpng</a>
		</figcaption>
	</figure>
</div>

[Télécharger l'intégralité du site RML12 avec tous les visuels](https://git.duniter.org/rml12/rml12.duniter.io/-/jobs/artifacts/master/download?job=pages)

## Relais médias existants

- [12ème Rencontres de la Monnaie Libre, club-presse-bordeaux.fr](http://www.club-presse-bordeaux.fr/communiques/12eme-rencontres-de-monnaie-libre/)
- [16/11, Rencontre Monnaie Libre, CAFE CAMPUS, Radio Campus Bordeaux](https://soundcloud.com/radiocampusbx/16-11-cafe-campus-revue-de-presse-des-hebdos-rencontre-des-monnaies-libres-les-samis#t=10:35)
- [LCDT 34 - Rencontre de la G1, Le chemin des transitions, La clé des ondes](https://audioblog.arteradio.com/blog/98702/podcast/125914)
- [LCDT 19 - 1ere Monnaie Libre, Le chemin des transitions, La clé des ondes](https://audioblog.arteradio.com/blog/98702/podcast/124416)
- [Découvrez la monnaie libre, gironde.demosphere.net](https://gironde.demosphere.net/search?search=d%C3%A9couvrez+la+monnaie+libre&allset=1&event-date=future&only-title=on)
- [Bordeaux Rencontres Monnaie Libre, L'Agenda du Libre](https://www.agendadulibre.org/events/18258)
- [Rencontres Monnaie Libre Bordeaux 2018, Blog de geekette](https://blog-de-geekette.com/blog/agenda/rencontres-monnaie-libre-bordeaux-2018/)

- [Rencontres Monnaie Libre 12, Évènement Facebook](https://www.facebook.com/events/999205830267839/)
- [#RML12 sur Diaspora*](https://framasphere.org/tags/rml12)
- [#RML12 sur Mastodon](https://mastodon.social/tags/rml12)
- [RML12 sur Twitter](https://twitter.com/search?q=rml12)
