# Ouverture sur le monde

Formations, accueil, transmission, partage, en interaction et complémentarité avec le territoire...
Nous sommes acteurs de l'économie locale, lieu de ressources et d'expérimentation de méthodes novatrices pour contribuer à un monde plus résilient et à l'essaimage d'autres initiatives.