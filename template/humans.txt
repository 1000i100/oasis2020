/* TEAM */
Author, Developpement, Design, Content : [1000i100] Millicent Billette de Villemeur
Location: Bordeaux, France.
Website: https://1forma-tic.fr/
Blog: https://blog.1000i100.fr/

/* THANKS */
Name: CynthiaSz https://github.com/CynthiaSz pour ses contributions à l'intégration du site.
Name: Loïc Mathey, Pi Nguyen pour leur conseil et création graphique.
Name: Jon Prepeluh, Corpus Delicti, Creative Stall, Maria Kislitsina, Jemis Mali, N.K.Narasimhan, antto, shashank singh, unlimicon, Vaibhav Radhakrishnan, Adrien Coquet, Gan Khoon Lay, Vladimir Belochkin et Mitchell Eva tous via The Noun Project pour les pictogrammes
Name: Marie Nau, Marie Lefort, Julia Schindler pour leurs précieuses relectures

/* SITE */
Last update: {build:__now}
Standards: HTML5, CSS3+Stylus, ES6(JavaScript)
Components: VanillaJS
Software: Gitlab, Gitlab-CI, WebStorm, Atom, Git, Inkscape, Gimp, Darktable
